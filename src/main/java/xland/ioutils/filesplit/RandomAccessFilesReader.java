package xland.ioutils.filesplit;

import xland.ioutils.filesplit.internal.Log;
import xland.ioutils.filesplit.internal.Mth;
import xland.ioutils.filesplit.internal.ThrowableContainer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class RandomAccessFilesReader implements CloseableRunnable {
    private final RandomAccessFile raf;
    private final OutputStream sep;
    private final int dbi;
    private final long mfs;

    RandomAccessFilesReader(RandomAccessFile raf, OutputStream sep, int dbi, long mfs) {
        this.raf = raf;
        this.sep = sep;
        this.dbi = dbi;
        this.mfs = mfs;
    }

    public static List<? extends Thread> createThreads(File input, List<OutputStream> outputSplits,
                                                       long maxFileSize)
                throws IOException{
        Log.debug("input: %s", input);

        List<RAFListableThread> threads = new ArrayList<>();
        long ptr = 0; maxFileSize -= 4;
        int maxFileCount = outputSplits.size();
        if (maxFileCount <= 8) {
            int i = 0;
            for (OutputStream os : outputSplits) {
                RandomAccessFile raf2 = new RandomAccessFile(input, "r");
                raf2.seek(ptr);
                threads.add(new RAFListableThread(Collections.singletonList(new RandomAccessFilesReader(
                        raf2, os, i, maxFileSize))));
                ptr += maxFileSize;  // fake
                i++;
            }
        } else {
            for (int i = 0; i < 8; i++)
                threads.add(new RAFListableThread(new ArrayList<>()));
            for (int i = 0; i < maxFileCount; i++) {
                OutputStream os = outputSplits.get(i);
                RandomAccessFile raf2 = new RandomAccessFile(input, "r");
                raf2.seek(ptr);
                int threadId = i % 8;
                threads.get(threadId).getThreads().add(new RandomAccessFilesReader(raf2, os, i, maxFileSize));
            }
        }
        return threads;
    }

    @Override
    public void close() throws IOException {
        sep.close();
        raf.close();
    }

    @Override
    public void run() {
        IOException e = null;
        try {
            byte[] buffer = new byte[8192];
            sep.write(Mth.getCommonHeader());
            Log.debug("output channel #%d is writing the bytes from raf" +
                    " after writing header.", dbi);
            int len;
            long restSize = mfs;
            while ((len = raf.read(buffer, 0, (int) Mth.clamp(restSize, 0L, 8192L))) >= 0) {
                restSize -= 8192;
                sep.write(buffer, 0, len);
            }
        } catch (IOException ex) {
            e = ex;
        } finally {
            try {

                this.close();
            } catch (IOException ex) {
                if (e == null) e = ex; else e.addSuppressed(ex);
            }
        } if (e != null) throw new ThrowableContainer(new IOException("An exception is caused in channel #" + dbi, e));
        Log.debug("Channel %d is finished.", dbi);
    }
}
