package xland.ioutils.filesplit.internal;

import java.io.PrintStream;
import java.util.function.Supplier;

@Internal
public class Log {
    private static Supplier<PrintStream> sysOut = () -> System.out, sysErr = () -> System.err;
    private static final boolean shouldDebug;
    static {
        var prop = System.getProperty("filesplit.debug");
         shouldDebug = "true".equalsIgnoreCase(prop)
                || "yes".equalsIgnoreCase(prop)
                || "on".equalsIgnoreCase(prop)
                || System.getenv("FILESPLIT_DEBUG") != null;
    }

    static {
        if (shouldDebug) System.out.println("filesplit.debug: true");
    }

    public static boolean shouldDebug() { return shouldDebug; }

    public static void debug(String s) {
        if (shouldDebug) log(s);
    }

    public static void debug(String s, Object p0) {
        if (shouldDebug) log(s, p0);
    }

    public static void debug(String s, Object p0, Object p1) {
        if (shouldDebug) log(s, p0, p1);
    }

    public static void debug(String s, Object p0, Object p1, Object p2) {
        if (shouldDebug) log(s, p0, p1, p2);
    }

    public static void debug(String s, Object... args) {
        if (shouldDebug) log(s, args);
    }

    public static void nonDebug(String s) {
        if (!shouldDebug) log(s);
    }

    public static void nonDebug(String s, Object p0) {
        if (!shouldDebug) log(s, p0);
    }

    public static void nonDebug(String s, Object p0, Object p1) {
        if (!shouldDebug) log(s, p0, p1);
    }

    public static void nonDebug(String s, Object p0, Object p1, Object p2) {
        if (!shouldDebug) log(s, p0, p1, p2);
    }

    public static void nonDebug(String s, Object... args) {
        if (!shouldDebug) log(s, args);
    }

    public static void log(String s) {
        sysOut.get().println(s);
    }

    public static void log(String s, Object p0) {
        Log.sysOut.get().printf(s, p0).println();
    }

    public static void log(String s, Object p0, Object p1) {
        Log.sysOut.get().printf(s, p0, p1).println();
    }

    public static void log(String s, Object p0, Object p1, Object p2) {
        Log.sysOut.get().printf(s, p0, p1, p2).println();
    }

    public static void log(String s, Object... args) {
        Log.sysOut.get().printf(s, args).println();
    }

    public static void error(String s) {
        sysErr.get().println(s);
    }

    public static void error(String s, Object p0) {
        Log.sysErr.get().printf(s, p0).println();
    }

    public static void error(String s, Object p0, Object p1) {
        Log.sysErr.get().printf(s, p0, p1).println();
    }

    public static void error(String s, Object p0, Object p1, Object p2) {
        Log.sysErr.get().printf(s, p0, p1, p2).println();
    }

    public static void printStackTraceWhenDebug(Throwable t) {
        if (shouldDebug) t.printStackTrace(Log.sysErr.get());
    }

    public static void error(String s, Object... args) {
        Log.sysErr.get().printf(s, args).println();
    }

    synchronized public static void setOut(Supplier<PrintStream> sysOut) { Log.sysOut = sysOut; }
    synchronized public static void setOut(PrintStream sysOut) { Log.sysOut = () -> sysOut; }
    synchronized public static void setErr(Supplier<PrintStream> sysErr) { Log.sysErr = sysErr; }
    synchronized public static void setErr(PrintStream sysErr) { Log.sysErr = () -> sysErr; }
    public static void setOut(Supplier<PrintStream> sysOut, Supplier<PrintStream> sysErr) {
        setOut(sysOut);
        setErr(sysErr);
    }
    public static void setOut(PrintStream sysOut, PrintStream sysErr) {
        setOut(sysOut);
        setOut(sysErr);
    }
}
