package xland.ioutils.filesplit.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.function.IntPredicate;

@Internal
public final class Mth {
    public static byte[] fromInt(int i) {
        return new byte[] {
                (byte) (i >> 24),
                (byte) (i >> 16),
                (byte) (i >> 8),
                (byte)  i
        };
    }

    private static final byte[] COMMON_HEADER, INFO_HEADER;
    static {
        COMMON_HEADER = fromInt(Constants.COMMON_HEADER);
        INFO_HEADER = fromInt(Constants.INFO_HEADER);
    }
    public static byte[] getCommonHeader() {
        return COMMON_HEADER.clone();
    }
    public static byte[] getInfoHeader() {
        return INFO_HEADER.clone();
    }

    public static int toInt(byte[] bs) {
        if (bs.length < 4) return -1;
        int[] intArray = new int[4];
        for (int i = 0; i < 4; ++i) {
            if (bs[i] < 0) intArray[i] = 256 + bs[i];
            else intArray[i] = bs[i];
        }
        return  (intArray[0] << 24) +
                (intArray[1] << 16) +
                (intArray[2] << 8)  +
                 intArray[3];
    }

    public static void writeString(OutputStream instance, String s) throws IOException {
        byte[] b = s.getBytes(StandardCharsets.UTF_8);
        instance.write(fromInt(b.length));
        instance.write(b);
    }

    public static int maxFileCount(long maxFileSize, long totalSize)
            throws IndexOutOfBoundsException {
        if (totalSize <= maxFileSize) return 1;
        long mfc = Math.floorDiv(totalSize, maxFileSize);
        if (totalSize % maxFileSize != 0)
            mfc++;
        if (mfc > Integer.MAX_VALUE || mfc <= 0)
            throw new IndexOutOfBoundsException("File count " +
                    mfc + " is out of" +
                    " bounds: [1, 2,147,483,647]");
        return (int) mfc;
    }

    public static long clamp(long num, long min, long max) {
        return Math.max(min, Math.min(max, num));
    }

    public static String readString(InputStream instance)
            throws IOException, IllegalArgumentException {
        byte[] b = new byte[4];
        if (instance.read(b, 0, 4) != 4) throw new IllegalArgumentException("Not a valid INFO file");
        int l = toInt(b);   // len
        b = new byte[l];
        if (instance.read(b, 0, l) != l) throw new IllegalArgumentException("Not a valid INFO file");
        return new String(b, StandardCharsets.UTF_8);
    }

    private Mth() {}
}
