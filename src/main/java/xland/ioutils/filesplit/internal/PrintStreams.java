package xland.ioutils.filesplit.internal;

import javax.annotation.Nullable;
import java.io.OutputStream;
import java.io.PrintStream;

@Internal
public class PrintStreams {
    public static final PrintStream nul = new PrintStream(OutputStream.nullOutputStream());
    public static PrintStream mix(@Nullable PrintStream x, @Nullable PrintStream y) {
        if (x == null)
            return y == null ? nul : y;
        // x != null
        return y == null ? x : new PrintStream(new OutputStream() {
            @Override
            public void write(int b) {
                x.write(b);
                y.write(b);
            }
        });
    }
}
