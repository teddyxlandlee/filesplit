package xland.ioutils.filesplit.internal;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.examples.Archiver;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarFile;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.UUID;

public class Archivers {
    private static final Archiver theArchiver = new Archiver();

    public static void compressTgz(File source, File target) throws IOException {
        File tmpFile = createTempFile();
        compressTar(source, tmpFile);
        compressGzip(tmpFile, target);
    }

    public static void compressTgz(Path source, Path target) throws IOException {
        Path tmpFile = createTempFile().toPath();
        compressTar(source, tmpFile);
        compressGzip(tmpFile, target);
    }

    public static void compressTar(File source, File target) throws IOException {
        if (source.isDirectory()) {
            try {
                theArchiver.create(new TarArchiveOutputStream(new BufferedOutputStream(new FileOutputStream(target))), source);
            } catch (ArchiveException e) {
                throw new InternalError(e);
            }
        } else {
            var filename = source.getName();
            var tarOut = new TarArchiveOutputStream(new BufferedOutputStream(new FileOutputStream(target)));
            var entry = tarOut.createArchiveEntry(source, filename);
            tarOut.putArchiveEntry(entry);
            new BufferedInputStream(new FileInputStream(source)).transferTo(tarOut);
            tarOut.closeArchiveEntry();
            tarOut.finish();
            tarOut.close();
        }
    }

    public static void compressTar(Path source, Path target) throws IOException {
        if (Files.isDirectory(source))
            theArchiver.create(new TarArchiveOutputStream(Files.newOutputStream(target)), source);
        else {
            var filename = source.getFileName().toString();
            var tarOut = new TarArchiveOutputStream(Files.newOutputStream(target));
            var entry = tarOut.createArchiveEntry(source, filename);
            tarOut.putArchiveEntry(entry);
            Files.copy(source, tarOut);
            tarOut.closeArchiveEntry();
            tarOut.finish();
            tarOut.close();
        }
    }

    public static void compressGzip(File source, File target) throws IOException {
        var gzip = new GzipCompressorOutputStream(new FileOutputStream(target));
        new BufferedInputStream(new FileInputStream(source)).transferTo(gzip);
    }

    public static void compressGzip(Path source, Path target) throws IOException {
        var gzip = new GzipCompressorOutputStream(Files.newOutputStream(target));
        Files.newInputStream(source).transferTo(gzip);
    }

    private static final class SevenZOutputStream extends OutputStream {
        final SevenZOutputFile out;

        SevenZOutputStream(SevenZOutputFile file) {
            this.out = file;
        }

        @Override
        public void write(int b) throws IOException {
            out.write(b);
        }

        @Override
        public void write(@Nonnull byte[] b) throws IOException {
            out.write(b);
        }

        @Override
        public void write(@Nonnull byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
        }
    }

    public static void compress7Z(File source, File target) throws IOException {
        if (source.isDirectory())
            theArchiver.create(new SevenZOutputFile(target), source);
        else {
            var filename = source.getName();
            var out = new SevenZOutputFile(target);
            var entry = out.createArchiveEntry(source, filename);
            out.putArchiveEntry(entry);
            new BufferedInputStream(new FileInputStream(source)).transferTo(new SevenZOutputStream(out));
            out.closeArchiveEntry();
            out.finish();
            out.close();
        }
    }

    public static void compress7Z(Path source, Path target) throws IOException {
        if (Files.isDirectory(source))
            theArchiver.create(new SevenZOutputFile(FileChannel.open(target)), source);
        else {
            var filename = source.getFileName().toString();
            var out = new SevenZOutputFile(FileChannel.open(target));
            var entry = out.createArchiveEntry(source, filename);
            out.putArchiveEntry(entry);
            Files.copy(source, new SevenZOutputStream(out));
            out.closeArchiveEntry();
            out.finish();
            out.close();
        }
    }

    public static void compressZip(File source, File target) throws IOException {
        if (source.isDirectory()) {
            try {
                theArchiver.create(new ZipArchiveOutputStream(target), source);
            } catch (ArchiveException e) {
                throw new InternalError(e);
            }
        } else {
            var filename = source.getName();
            var zipOut = new ZipArchiveOutputStream(target);
            var entry = zipOut.createArchiveEntry(source, filename);
            zipOut.putArchiveEntry(entry);
            new BufferedInputStream(new FileInputStream(source)).transferTo(zipOut);
            zipOut.closeArchiveEntry();
            zipOut.finish();
            zipOut.close();
        }
    }

    public static void compressZip(Path source, Path target) throws IOException {
        if (Files.isDirectory(source))
            theArchiver.create(new ZipArchiveOutputStream(target), source);
        else {
            var filename = source.getFileName().toString();
            var zipOut = new ZipArchiveOutputStream(target);
            var entry = zipOut.createArchiveEntry(source, filename);
            zipOut.putArchiveEntry(entry);
            Files.copy(source, zipOut);
            zipOut.closeArchiveEntry();
            zipOut.finish();
            zipOut.close();
        }
    }

    public static void decompressTgz(File source, File target) throws IOException {
        File tmpFile = createTempFile();
        decompressGzip(source, tmpFile);
        decompressTar(tmpFile, target);
    }

    public static void decompressTgz(Path source, Path target) throws IOException {
        Path tmpFile = createTempFile().toPath();
        decompressGzip(source, tmpFile);
        decompressTar(tmpFile, target);
    }

    public static void decompressTar(Path source, Path target) throws IOException {
        TarFile tarFile = new TarFile(source);
        for (TarArchiveEntry entry : tarFile.getEntries()) {
            Path path = target.resolve(entry.getName());
            if (!entry.isDirectory()) {
                Files.createDirectories(path.getParent());

                InputStream inputStream = tarFile.getInputStream(entry);
                OutputStream outputStream = Files.newOutputStream(path);

                inputStream.transferTo(outputStream);
                inputStream.close();
                outputStream.close();
            } else {
                Files.createDirectories(path);
            }
        }
    }

    public static void decompressTar(File source, File target) throws IOException {
        decompressTar(source.toPath(), target.toPath());
    }

    public static void decompressGzip(File source, File target) throws IOException {
        var gzip = new GzipCompressorInputStream(new FileInputStream(source));
        gzip.transferTo(new BufferedOutputStream(new FileOutputStream(target)));
    }

    public static void decompressGzip(Path source, Path target) throws IOException {
        var gzip = new GzipCompressorInputStream(Files.newInputStream(source));
        gzip.transferTo(Files.newOutputStream(target));
    }

    public static void decompress7Z(Path source, Path target) throws IOException {
        SevenZFile sevenZFile = new SevenZFile(FileChannel.open(source));
        for (SevenZArchiveEntry entry : sevenZFile.getEntries()) {
            Path path = target.resolve(entry.getName());
            if (!entry.isDirectory()) {
                Files.createDirectories(path.getParent());

                InputStream inputStream = sevenZFile.getInputStream(entry);
                OutputStream outputStream = Files.newOutputStream(path);

                inputStream.transferTo(outputStream);
                inputStream.close();
                outputStream.close();
            } else {
                Files.createDirectories(path);
            }
        }
    }

    public static void decompress7Z(File source, File target) throws IOException {
        decompress7Z(source.toPath(), target.toPath());
    }

    public static void decompressZip(Path source, @MayNotExist Path target) throws IOException {
        ZipFile zipFile = new ZipFile(FileChannel.open(source));
        Enumeration<ZipArchiveEntry> entries = zipFile.getEntries();

        if (!entries.hasMoreElements()) return;  // There is nothing in the zip file
        var firstEntry = entries.nextElement();

        Files.createDirectories(target.getParent());
        if (!entries.hasMoreElements()) {
            // firstEntry -> target
            if (!firstEntry.isDirectory()) {
                // target: new file
                decompressZipEntry(zipFile, firstEntry, target);
                return;
            }
        }

        while (entries.hasMoreElements()) {
            ZipArchiveEntry entry = entries.nextElement();
            Path path = target.resolve(entry.getName());
            if (!entry.isDirectory()) {
                Files.createDirectory(target);

                decompressZipEntry(zipFile, entry, path);
            } else {
                Files.createDirectories(path);
            }
        } zipFile.close();
    }

    private static void decompressZipEntry(ZipFile zipFile, ZipArchiveEntry entry,
                                           Path path) throws IOException {
        InputStream inputStream = zipFile.getInputStream(entry);
        OutputStream outputStream = Files.newOutputStream(path);
        inputStream.transferTo(outputStream);
        inputStream.close();
        outputStream.close();
    }

    public static void decompressZip(File source, File target) throws IOException {
        decompressZip(source.toPath(), target.toPath());
    }

    public static void copy(Path source, Path target) throws IOException {
        Files.copy(source, target);
    }

    public static void copy(File source, File target) throws IOException {
        copy(source.toPath(), target.toPath());
    }

    @Internal
    public static File createTempFile() throws IOException {
        File f = File.createTempFile("fsplit-{" + UUID.randomUUID() + '}',
                null);
        if (!"true".equalsIgnoreCase(System.getProperty("filesplit.debug.tmpfile")))
            f.deleteOnExit();
        return f;
    }
}
