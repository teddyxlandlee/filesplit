package xland.ioutils.filesplit.internal;

import xland.ioutils.filesplit.ArchiveType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.PrintStream;
import java.util.Objects;

@Internal
// Bean
public class EncodeArgs {
    private long subfileSize = Constants.DEFAULT_SUBFILE_SIZE;
    /*@Nonnull*/
    private ArchiveType archiveType = Constants2.DEFAULT_ARCHIVE_TYPE;

    public void setSubfileSize(String string) {
        try {
            this.setSubfileSize(maxOneFileSize0(string).longValue());
        } catch (NumberFormatException e) {
            Log.error("Invalid file size: " + string + "." +
                    " Will be set to default (99.4M).");
        }
    }

    private static Number maxOneFileSize0(String string) throws NumberFormatException {
        return maxOneFileSize1(string, false);
    }

    private static Number maxOneFileSize1(String string, boolean disableEndWithB) throws NumberFormatException {
        final int ln = string.length() - 1;
        String sub = string.substring(0, ln);
        switch (string.charAt(ln)) {
            case 'B':
                if (disableEndWithB)
                    throw new NumberFormatException("Double 'B' in the end");
                return maxOneFileSize1(string, true);
            case 'K':
                return Double.parseDouble(sub) * 1024;
            case 'M':
                return Double.parseDouble(sub) * (1024 * 1024);
            case 'G':
                return Double.parseDouble(sub) * (1024 * 1024 * 1024);
            default:
                return Long.parseLong(string);
        }
    }

    /*GENERATED*/

    public long getSubfileSize() {
        return subfileSize;
    }

    public void setSubfileSize(long subfileSize) {
        this.subfileSize = subfileSize;
    }

    @Nonnull
    public ArchiveType getArchiveType() {
        return archiveType;
    }

    public void setArchiveType(@Nonnull ArchiveType archiveType) {
        Objects.requireNonNull(archiveType);
        this.archiveType = archiveType;
    }

    public EncodeArgs(long subfileSize, @Nonnull ArchiveType archiveType) {
        Objects.requireNonNull(archiveType);
        this.subfileSize = subfileSize;
        this.archiveType = archiveType;
    }

    public EncodeArgs() {}

    public EncodeArgs(long subfileSize) {
        this.subfileSize = subfileSize;
    }

    public EncodeArgs(@Nonnull ArchiveType archiveType) {
        Objects.requireNonNull(archiveType);
        this.archiveType = archiveType;
    }
}
