package xland.ioutils.filesplit.internal;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Internal
@Documented
@Retention(RetentionPolicy.CLASS)
public @interface Internal {
}
