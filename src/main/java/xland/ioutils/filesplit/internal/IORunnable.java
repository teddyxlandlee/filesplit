package xland.ioutils.filesplit.internal;

import xland.ioutils.filesplit.info.InvalidFileSplitInfoException;

import java.io.IOException;

@FunctionalInterface
@Internal
public interface IORunnable {
    void run() throws IOException, InvalidFileSplitInfoException;
}
