package xland.ioutils.filesplit.internal;

import javax.annotation.Nonnull;
import java.util.Objects;

@Internal
public final class ThrowableContainer extends RuntimeException {
    public ThrowableContainer(@Nonnull Throwable t) {
        super(t);
    }

    @Override @Nonnull
    public Throwable getCause() {
        Throwable cause = super.getCause();
        Objects.requireNonNull(cause);
        return cause;
    }
}
