package xland.ioutils.filesplit;

import xland.ioutils.filesplit.internal.IORunnable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DecodeProcessor extends AbstractDecodeProcessor<Path> implements IORunnable {
    protected DecodeProcessor(Path source, Path outputRoot) {
        super(source, outputRoot);
    }

    @Override
    public InputStream getFileSplitInfo() throws IOException {
        Path infoFile = source.resolve("INFO.fsplitinfo");
        return Files.newInputStream(infoFile);
    }

    @Override
    public List<InputStream> getParts(int maxFileCount) throws IOException{
        List<InputStream> inputStreams = new ArrayList<>();
        for (int i = 0; i < maxFileCount; i++) {
            Path path = source.resolve(i + ".fsplit");
            inputStreams.add(Files.newInputStream(path));
        } return inputStreams;
    }

    public static DecodeProcessor of(Path source, Path outputRoot) {
        return new DecodeProcessor(source, outputRoot);
    }

}
