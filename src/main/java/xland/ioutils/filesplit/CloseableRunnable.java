package xland.ioutils.filesplit;

interface CloseableRunnable
extends java.io.Closeable, Runnable {}
