package xland.ioutils.filesplit.info;

import xland.ioutils.filesplit.internal.Mth;
import xland.ioutils.filesplit.internal.DefaultArchiveTypes;

import java.io.IOException;
import java.io.InputStream;

public class V1InfoFactory implements FileSplitInfoFactory {
    @Override
    public boolean matches(int version) {
        return version == 1;
    }

    @Override
    public FileSplitInfo fromInputStream(InputStream inputStream)
            throws IOException, InvalidFileSplitInfoException {
        byte[] cache = new byte[4];
        if (inputStream.read(cache, 0, 4) < 4)
            throw InvalidFileSplitInfoException.maxFileCount(cache);
        int maxFileCount = Mth.toInt(cache);
        String filename = Mth.readString(inputStream);
        return FileSplitInfo.of(maxFileCount, filename, DefaultArchiveTypes.NOP);
    }
}
