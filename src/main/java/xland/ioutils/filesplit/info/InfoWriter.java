package xland.ioutils.filesplit.info;

import xland.ioutils.filesplit.internal.Constants;
import xland.ioutils.filesplit.internal.IORunnable;
import xland.ioutils.filesplit.internal.Mth;

import java.io.IOException;
import java.io.OutputStream;

/* @version 3 */
public class InfoWriter implements IORunnable {
    private final OutputStream outputStream;
    private final FileSplitInfo info;

    public InfoWriter(OutputStream outputStream, FileSplitInfo info) {
        this.outputStream = outputStream;
        this.info = info;
    }

    @Override
    public void run() throws IOException {
        try {
            outputStream.write(Mth.getInfoHeader());
            outputStream.write(Constants.INFO_VERSION);
            outputStream.write(Mth.fromInt(info.maxFileCount()));
            Mth.writeString(outputStream, info.filename());
            outputStream.write(info.archiveType().getId());
        } finally {
            outputStream.close();
        }
    }
}
