package xland.ioutils.filesplit.info;

import xland.ioutils.filesplit.ArchiveType;
import xland.ioutils.filesplit.internal.Mth;

import java.io.IOException;
import java.io.InputStream;

public class V2_3InfoFactory implements FileSplitInfoFactory {
    @Override
    public boolean matches(int version) {
        return version == 2 || version == 3;
    }

    @Override
    public FileSplitInfo fromInputStream(InputStream inputStream)
            throws IOException, InvalidFileSplitInfoException {
        byte[] cache = new byte[4];
        if (inputStream.read(cache, 0, 4) < 4)
            throw InvalidFileSplitInfoException.maxFileCount(cache);
        int maxFileCount = Mth.toInt(cache);
        String filename = Mth.readString(inputStream);
        var atId = inputStream.read();
        var archiveType = ArchiveType
                .fromId(atId).orElseThrow(() ->
                        InvalidFileSplitInfoException.missingArchiveType(atId));
        return FileSplitInfo.of(maxFileCount, filename, archiveType);
    }
}
