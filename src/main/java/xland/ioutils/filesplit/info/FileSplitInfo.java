package xland.ioutils.filesplit.info;

import xland.ioutils.filesplit.ArchiveType;

public class FileSplitInfo {
    private final int maxFileCount;
    private final String filename;
    private final ArchiveType archiveType;

    public FileSplitInfo(int maxFileCount,
                         String filename,
                         ArchiveType archiveType) {
        this.maxFileCount = maxFileCount;
        this.filename = filename;
        this.archiveType = archiveType;
    }

    public int maxFileCount() {
        return maxFileCount;
    }

    public String filename() {
        return filename;
    }

    public ArchiveType archiveType() {
        return archiveType;
    }

    public static FileSplitInfo of(int maxFileCount,
                                   String filename,
                                   ArchiveType archiveType) {
        return new FileSplitInfo(maxFileCount, filename, archiveType);
    }
}
