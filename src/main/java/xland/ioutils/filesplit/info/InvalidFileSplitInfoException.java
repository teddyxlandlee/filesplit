package xland.ioutils.filesplit.info;

import xland.ioutils.filesplit.internal.Mth;
import xland.ioutils.filesplit.internal.Internal;

public class InvalidFileSplitInfoException extends Exception {
    public InvalidFileSplitInfoException() { super(); }
    public InvalidFileSplitInfoException(String message) { super(message); }
    public InvalidFileSplitInfoException(Throwable cause) { super(cause); }
    public InvalidFileSplitInfoException(String message, Throwable cause) { super(message, cause); }

    @Internal
    static InvalidFileSplitInfoException version(byte[] b) {
        return new InvalidFileSplitInfoException("Invalid file header: "
                + Integer.toHexString(Mth.toInt(b)));
    }

    @Internal
    static InvalidFileSplitInfoException maxFileCount(byte[] b) {
        return new InvalidFileSplitInfoException("Invalid max file count: "
                + Mth.toInt(b));
    }

    @Internal
    static InvalidFileSplitInfoException missingInfoFactory(int version) {
        return new InvalidFileSplitInfoException("Missing file split info factory" +
                " for version " + version + '.');
    }

    @Internal
    //@Deprecated(forRemoval = false)
    static InvalidFileSplitInfoException missingArchiveType(int archiveTypeId) {
        return new InvalidFileSplitInfoException(String.format("Missing archive " +
                "type (%02x).", archiveTypeId));
    }
}
