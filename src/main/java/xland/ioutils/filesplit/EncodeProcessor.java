package xland.ioutils.filesplit;

import xland.ioutils.filesplit.internal.EncodeArgs;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class EncodeProcessor extends AbstractEncodeProcessor<Path> {
    protected EncodeProcessor(Path source, Path target, EncodeArgs encodeArgs) {
        super(source, target, encodeArgs);
    }

    @Override
    public OutputStream createFileSplitInfo() throws IOException {
        Path infoFile = target.resolve("INFO.fsplitinfo");
        return Files.newOutputStream(infoFile);
    }

    @Override
    public List<OutputStream> createParts(int maxFileCount) throws IOException {
        List<OutputStream> outputStreams = new ArrayList<>();
        for (int i = 0; i < maxFileCount; i++) {
            Path path = target.resolve(i + ".fsplit");
            outputStreams.add(Files.newOutputStream(path));
        } return outputStreams;
    }

    public static EncodeProcessor of(Path source, Path outputRoot, EncodeArgs encodeArgs) {
        return new EncodeProcessor(source, outputRoot, encodeArgs);
    }

    @Override
    public void initializeTarget() throws IOException {
        Files.createDirectories(target);
        if (Files.exists(target.resolve("INFO.fsplitinfo"))) {
            throw new IOException("Directory " + target + " already contains filesplit " +
                    "files. Please move/remove them before processing.");
        }
    }
}
