package xland.ioutils.filesplit;

import xland.ioutils.filesplit.internal.Constants;
import xland.ioutils.filesplit.internal.Log;
import xland.ioutils.filesplit.internal.Mth;
import xland.ioutils.filesplit.internal.ThrowableContainer;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class RandomAccessFilesWriter implements CloseableRunnable {
    private final RandomAccessFile raf;
    private final InputStream sep;
    private final int dbi;

    RandomAccessFilesWriter(RandomAccessFile raf, InputStream inputStream, int dbi) {
        this.raf = raf;
        this.sep = inputStream;
        this.dbi = dbi;
    }

    public static List<? extends Thread> createThreads(File output, List<InputStream> inputSplits)
                throws IOException {
        Log.debug("output: %s", output);

        List<RAFListableThread> threads = new ArrayList<>();
        int ptr = 0;
        int maxFileCount = inputSplits.size();
        if (maxFileCount <= 8) {
            for (int i = 0; i < maxFileCount; i++) {
                InputStream is1 = inputSplits.get(i);
                if (incorrect(is1)) throw new IOException("Bad file header in FileSplit part #" + i);

                int rest = is1.available();
                Log.debug("Channel #%d is available. There are %d available bytes.", i, rest);

                RandomAccessFile raf2 = new RandomAccessFile(output, "rw");
                raf2.seek(ptr);
                threads.add(new RAFListableThread(Collections.singletonList(new RandomAccessFilesWriter(raf2, is1, i))));
                ptr += rest;
            }
        } else {
            for (int i = 0; i < 8; i++)
                threads.add(new RAFListableThread(new ArrayList<>()));
            for (int i = 0; i < maxFileCount; i++) {
                InputStream is1 = inputSplits.get(i);
                if (incorrect(is1)) throw new IOException("Bad file header in FileSplit part #" + i);

                int rest = is1.available();
                Log.debug("Channel #%d is available. There are %d available bytes.", i, rest);

                RandomAccessFile raf2 = new RandomAccessFile(output, "rw");
                raf2.seek(ptr);
                int threadId = i % 8;
                threads.get(threadId).getThreads().add(new RandomAccessFilesWriter(raf2, is1, i));
                //threads.add(new Single(new RandomAccessFiles(raf2, is1)));
                ptr += rest;
            }
        }
        return threads;
    }

    private static boolean incorrect(InputStream inputStream1) throws IOException {
        byte[] b = new byte[4];
        return inputStream1.read(b, 0, 4) != 4 || Mth.toInt(b) == Constants.COMMON_HEADER;
    }

    @Override
    public void run() {
        IOException e = null;
        try {
            sep.transferTo(new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                    raf.write(b);
                }

                @Override
                public void write(@Nonnull byte[] b) throws IOException {
                    raf.write(b);
                }

                @Override
                public void write(@Nonnull byte[] b, int off, int len) throws IOException {
                    raf.write(b, off, len);
                }
            });
        } catch (IOException ex) {
            e = ex;
        } finally {
            try {
                this.close();
            } catch (IOException ex) {
                if (e == null) e = ex; else e.addSuppressed(ex);
            }
        } if (e != null) throw new ThrowableContainer(new IOException("An exception is caused in channel #" + dbi, e));
        Log.debug("Channel %d is finished.", dbi);
    }

    @Override
    public void close() throws IOException {
        sep.close();
        raf.close();
    }
}
