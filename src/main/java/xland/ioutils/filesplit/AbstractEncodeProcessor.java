package xland.ioutils.filesplit;

import xland.ioutils.filesplit.info.FileSplitInfo;
import xland.ioutils.filesplit.info.InfoWriter;
import xland.ioutils.filesplit.internal.*;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public abstract class AbstractEncodeProcessor<O> implements IORunnable {
    protected final Path source;    // TODO support encode files in zips or other filesystems
    protected final O target;
    protected final EncodeArgs encodeArgs;

    protected AbstractEncodeProcessor(Path source, O target,
                                      EncodeArgs encodeArgs) {
        this.source = source;
        this.target = target;
        this.encodeArgs = encodeArgs;
    }

    @Override
    public void run() throws IOException {
        Log.log("Try encoding...");
        this.initializeTarget();

        File realSource;
        ArchiveType archiveType = encodeArgs.getArchiveType();
        if (!archiveType.acceptDirectory() && Files.isDirectory(source))
            throw new UnsupportedOperationException("Archive type " + archiveType.getName() +
                    " is unavailable for directory " + source + '.');
        Log.debug("Archive Type: [%s]", archiveType.getName());
        if (archiveType.shouldCopy()) {
            Log.debug("Archiving...");
            realSource = Archivers.createTempFile();
            archiveType.pathCompressor().process(source, realSource.toPath());
            Log.debug("Archiving finished");
        } else realSource = source.toFile();

        var filename = source.getFileName().toString(); // TODO: DIY filename by end user

        // count max_file_count with max_file_size
        var maxFileSize = encodeArgs.getSubfileSize();
        var totalSize = realSource.length();

        final int maxFileCount = Mth.maxFileCount(maxFileSize, totalSize);
        Log.debug("Max subfile count: %d from max subfile size (%d) and total size (%d)",
                maxFileCount, maxFileSize, totalSize);
        var parts = this.createParts(maxFileCount);
        Log.debug("Created file parts");
        var threads = RandomAccessFilesReader.createThreads(
                realSource, parts, maxFileSize
        );

        Log.debug("Prepare running threads...");
        for (Thread t: threads) {
            t.start();
        }
        try {
            for (Thread t: threads)
                t.join();
        } catch (InterruptedException e) {
            throw new ThrowableContainer(e);
        }
        var infoOutput = this.createFileSplitInfo();
        var fsInfo = FileSplitInfo.of(maxFileCount, filename, archiveType);
        new InfoWriter(infoOutput, fsInfo).run();
    }

    public void initializeTarget() throws IOException {}
    public abstract OutputStream createFileSplitInfo() throws IOException;
    public abstract List<OutputStream> createParts(int maxFileCount) throws IOException;
}
