package xland.ioutils.filesplit.console;

import xland.ioutils.filesplit.ArchiveType;
import xland.ioutils.filesplit.DecodeProcessor;
import xland.ioutils.filesplit.EncodeProcessor;
import xland.ioutils.filesplit.internal.ThrowableContainer;
import xland.ioutils.filesplit.internal.Constants;
import xland.ioutils.filesplit.internal.EncodeArgs;
import xland.ioutils.filesplit.internal.IORunnable;
import xland.ioutils.filesplit.internal.Log;

import java.nio.file.Path;
import java.util.Locale;

public class ConsoleApp {
    public static void main(String[] args) {
        long l = System.nanoTime();
        if (args.length < 2) help();

        IORunnable runnable;
        switch (args[0]) {
            case "encode":
                runnable = encoder(args);
                break;
            case "decode":
                runnable = decoder(args);
                break;
            default:
                help();
                return;
        }
        String s3 = "";
        try {
            runnable.run();
        } catch (Throwable t0) {
            Throwable t = t0;
            if (t instanceof ThrowableContainer) t = t.getCause();
            s3 = "un";
            //System.err.print("An exception occurred while processing: ");
            Log.log("An exception occurred while processing: ");
            Log.printStackTraceWhenDebug(t);
            Log.nonDebug("\t- %s\nTry running with argument '-x'", t);
        } finally {
            Log.log("Process finished %2$s" +
                      "successfully in %1$.2fms.", (System.nanoTime() - l) / 1e6, s3);
        }

    }

    static IORunnable encoder(String[] args) {
        Path source = Path.of(args[1]);
        Path output = Path.of(System.getProperty("user.dir"),
                "filesplit-" + source.getFileName());


        final EncodeArgs encodeArgs = new EncodeArgs();
        final var l = args.length-1;
        for (int i = 2; i < l; i++) {
            String s = args[i];
            if (s.startsWith("--")) {
                switch (s.substring(2).toLowerCase(Locale.ROOT)) {
                    case "size":
                        encodeArgs.setSubfileSize(args[++i]);
                        //subfileSize = maxOneFileSize(args[++i]);
                        break;
                    case "output":
                        output = Path.of(args[++i]);
                        break;
                    case "archive-type":
                        ArchiveType.fromName(args[++i]).ifPresent(encodeArgs::setArchiveType);
                        break;
                }
            }

        } return EncodeProcessor.of(source, output, encodeArgs);
    }

    static IORunnable decoder(String[] args) {
        Path source = Path.of(args[1]);
        Path outputRoot = Path.of(System.getProperty("user.dir"));
        var l = args.length-1;
        for (int i = 2; i < l; i++) {

            // overwrite them if add arguments
            String s = args[i];
            if (s.equalsIgnoreCase("--output")) {
                outputRoot = Path.of(args[++i]);
            }

        }
        return DecodeProcessor.of(source, outputRoot);
    }

    private static void help() {
        Log.log(Constants.HELP_MESSAGES);
        System.exit(0);
    }
}
