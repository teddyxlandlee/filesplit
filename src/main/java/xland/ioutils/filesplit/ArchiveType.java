package xland.ioutils.filesplit;

import xland.ioutils.filesplit.internal.DefaultArchiveTypes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

public interface ArchiveType {
    FileProcessor fileCompressor();

    PathProcessor pathCompressor();

    FileProcessor fileDecompressor();

    PathProcessor pathDecompressor();

    default boolean shouldCopy() {
        return true;
    }

    default boolean acceptDirectory() {
        return true;
    }

    byte getId();

    default String getName() {
        return this.toString();
    }


    @FunctionalInterface
    interface FileProcessor {
        void process(File source, File target) throws IOException;
    }

    @FunctionalInterface
    interface PathProcessor {
        void process(Path source, Path target) throws IOException;
    }

    static Optional<? extends ArchiveType> fromName(String s) {
        // TODO: spi - String->ArchiveType provider
        return DefaultArchiveTypes.fromName(s);
    }

    static Optional<? extends ArchiveType> fromId(byte id) {
        // TODO: spi - id->ArchiveType provider
        return Optional.ofNullable(DefaultArchiveTypes.fromId(id));
    }

    static Optional<? extends ArchiveType> fromId(int id) {
        return fromId((byte) id);
    }
}
