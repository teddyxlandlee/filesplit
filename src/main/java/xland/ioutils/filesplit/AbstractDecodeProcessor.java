package xland.ioutils.filesplit;

import xland.ioutils.filesplit.info.FileSplitInfo;
import xland.ioutils.filesplit.info.FileSplitInfoFactory;
import xland.ioutils.filesplit.info.InvalidFileSplitInfoException;
import xland.ioutils.filesplit.internal.Archivers;
import xland.ioutils.filesplit.internal.IORunnable;
import xland.ioutils.filesplit.internal.Log;
import xland.ioutils.filesplit.internal.ThrowableContainer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public abstract class AbstractDecodeProcessor<I>
        implements IORunnable {
    protected final I source;
    protected final Path outputRoot;

    protected AbstractDecodeProcessor(I source, Path outputRoot) {
        this.source = source;
        this.outputRoot = outputRoot;
    }

    @Override
    public void run() throws IOException, InvalidFileSplitInfoException {
        Log.log("Try decoding...");
        Files.createDirectories(outputRoot);

        FileSplitInfo info = FileSplitInfoFactory.readInfo(this.getFileSplitInfo());
        ArchiveType archiveType = info.archiveType();

        Path target0 = outputRoot.resolve(info.filename());
        File target = target0.toFile();

        if (!Files.exists(target0)) {
            Files.createFile(target0);
        } else {
            throw new IOException("Target file " + target0 + " already exists");
        }   //TODO: use dynamic filename: <filename> (1) if <filename> already exists.

        if (!archiveType.shouldCopy()) {
            decode0(info, target);
        } else {
            Log.debug("Decoding %s into temp file", info.filename());
            var tmp = Archivers.createTempFile();
            decode0(info, tmp);
            Log.debug("Decompressing %s... into %s", tmp, target0);
            info.archiveType().pathDecompressor().process(tmp.toPath(), target0);
        }

        Log.log("Output file: %s", target);
    }

    private void decode0(FileSplitInfo info, File outputFile) throws IOException {
        var filename = info.filename();
        Log.debug("Starting decode %s to %s", filename, outputRoot);

        int maxFileCount = info.maxFileCount();
        List<InputStream> parts = getParts(maxFileCount);
        List<? extends Thread> threads = RandomAccessFilesWriter.createThreads(outputFile, parts);
        for (Thread t: threads) {
            t.start();
        }
        try {
            for (Thread t : threads)
                t.join();
        } catch (InterruptedException e) {
            throw new ThrowableContainer(e);
        }
    }

    public abstract InputStream getFileSplitInfo() throws IOException;
    public abstract List<InputStream> getParts(int maxFileCount) throws IOException;
}
