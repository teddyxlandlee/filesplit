package xland.ioutils.filesplit;

import xland.ioutils.filesplit.internal.Log;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

class RAFListableThread extends Thread implements Closeable {
    private final List<CloseableRunnable> threads;

    protected RAFListableThread(List<CloseableRunnable> threads) {
        this.threads = threads;
    }

    public List<CloseableRunnable> getThreads() {
        return threads;
    }

    @Override
    public void run() {
        for (Runnable runnable : threads) {
            Log.debug("%s is running 'thread' %s", this, runnable);
            runnable.run();
        }
    }

    @Override
    public void close() throws IOException {
        for (Closeable runnable : threads)
            runnable.close();
    }
}
